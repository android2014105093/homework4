package com.example.domi.myapplication;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DefinitionFragment extends Fragment {

    final static String ARG_POSITION = "position";
    int mCurrentPosition = -1;
    TextView def;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            mCurrentPosition = savedInstanceState.getInt(ARG_POSITION);
        }
        View fragmentView = inflater.inflate(R.layout.fragment_definition, container, false);
        def = (TextView) fragmentView.findViewById(R.id.definition);
        return fragmentView;
    }

    @Override
    public void onStart() {
        Log.i("DefinitionFragment", "onStart");
        super.onStart();

        Bundle args = getArguments();
        if (args != null) {
            updateDefinitionView(args.getInt(ARG_POSITION));
        } else if (mCurrentPosition != -1) {
            updateDefinitionView(mCurrentPosition);
        }
    }

    public void updateDefinitionView(int position) {
        def.setText(Data.definitions[position]);
        mCurrentPosition = position;
    }

    @Override
    public void onSaveInstanceState(Bundle outstate) {
        super.onSaveInstanceState(outstate);
        outstate.putInt(ARG_POSITION, mCurrentPosition);
    }

    @Override
    public void onAttach(Activity activity) {
        Log.i("DefinitionFragment", "onAttach");
        super.onAttach(activity);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        Log.i("DefinitionFragment", "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume(){
        Log.i("DefinitionFragment", "onResume");
        super.onResume();
    }

    @Override
    public void onPause(){
        Log.i("DefinitionFragment", "onPause");
        super.onPause();
    }

    @Override
    public void onStop(){
        Log.i("DefinitionFragment", "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        Log.i("DefinitionFragment", "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        Log.i("DefinitionFragment", "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        Log.i("DefinitionFragment", "onDetach");
        super.onDetach();
    }
}
