package com.example.domi.myapplication;


import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class WordsFragment extends ListFragment {
    OnWordSelectedListener mCallback;

    public interface OnWordSelectedListener {
        public void onWordSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("WordsFragment", "onCreate");
        super.onCreate(savedInstanceState);
        int layout = Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                android.R.layout.simple_list_item_activated_1 :
                android.R.layout.simple_list_item_1;

        setListAdapter(new ArrayAdapter<String>(getActivity(), layout, Data.words));
    }

    @Override
    public void onStart() {
        Log.i("WordsFragment", "onStart");
        super.onStart();
        if (getFragmentManager().findFragmentById(R.id.definition_fragment) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnWordSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnWordSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mCallback.onWordSelected(position);
        getListView().setItemChecked(position, true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i("WordsFragment", "onCreateView");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        Log.i("WordsFragment", "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume(){
        Log.i("WordsFragment", "onResume");
        super.onResume();
    }

    @Override
    public void onPause(){
        Log.i("WordsFragment", "onPause");
        super.onPause();
    }

    @Override
    public void onStop(){
        Log.i("WordsFragment", "onStop");
        super.onStop();
    }

    @Override
    public void onDestroyView(){
        Log.i("WordsFragment", "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy(){
        Log.i("WordsFragment", "onDestroy");
        super.onDestroy();
    }

    @Override
    public void onDetach(){
        Log.i("WordsFragment", "onDetach");
        super.onDetach();
    }
}
